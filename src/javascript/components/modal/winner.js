import { showModal } from './modal'

export function showWinnerModal(fighter) {
  // call showModal function
  showModal({ title : 'Winner', bodyElement: `Player ${fighter.name} win!`, onClose: () => document.location.reload()})
}
