import { controls } from '../../constants/controls';

export async function fight(firstFighter, secondFighter) {
  return new Promise((resolve) => {
    // resolve the promise with the winner when fight is over

    const pressKey = [];

    firstFighter.canCriticalHit = true; 
    secondFighter.canCriticalHit = true;

    firstFighter.position = 'left';
    secondFighter.position = 'right';

    firstFighter.currentHealth = firstFighter.health;
    secondFighter.currentHealth = secondFighter.health; 

    document.addEventListener('keydown', (event) => {
      const keyCode = event.code;
      pressKey.push(event.code);
      checkPressKey(pressKey, firstFighter, secondFighter);

      if(firstFighter.currentHealth <= 0) {
        resolve(secondFighter);
      }else if(secondFighter.currentHealth <= 0){
        resolve(firstFighter);
      }
    });

    document.addEventListener('keyup', (event) => {
      const keyCode = event.code;
      let index = pressKey.indexOf(keyCode);
      if (index !== -1) pressKey.splice(index, 1);
    });
  });
}

export function getDamage(attacker, defender) {
  // return damage
  const damage = getHitPower(attacker) - getBlockPower(defender);
  return damage > 0 ? damage : 0;
}

export function getCriticalDamage(attacker, defender) {
  // return damage
  const damage = 2 * getHitPower(attacker);
  criticalHitTimeOut(attacker);
  return damage;
}

export function getHitPower(fighter) {
  // return hit power
  const criticalHitChance = getRandomNumber(1, 2);
  const power = fighter.attack * criticalHitChance;

  return power;
}

export function getBlockPower(fighter) {
  // return block power
  const dodgeChance = getRandomNumber(1, 2);
  const power = fighter.defense * dodgeChance;

  return power;
}

function getRandomNumber(min, max){
  return Math.random() * (max - min) + min;
}

function checkPressKey(key, firstFighter, secondFighter){
  if(!key.includes(controls.PlayerOneBlock) && key.includes(controls.PlayerOneAttack) && !key.includes(controls.PlayerTwoBlock)){
    updateFighterHealth(secondFighter, getDamage(firstFighter, secondFighter), secondFighter.position);
  }else if(!key.includes(controls.PlayerTwoBlock) && key.includes(controls.PlayerTwoAttack) && !key.includes(controls.PlayerOneBlock)){
    updateFighterHealth(firstFighter, getDamage(secondFighter, firstFighter), firstFighter.position);
  }else if(key.includes(...controls.PlayerOneCriticalHitCombination) && firstFighter.canCriticalHit){
    updateFighterHealth(secondFighter, getCriticalDamage(firstFighter, secondFighter), secondFighter.position)
  }else if(key.includes(...controls.PlayerTwoCriticalHitCombination) && secondFighter.canCriticalHit){
    updateFighterHealth(firstFighter, getCriticalDamage(secondFighter, firstFighter), firstFighter.position)
  }
}

function updateFighterHealth(fighter, damage, playerPosition){
  const healthBar = document.getElementById(`${playerPosition}-fighter-indicator`);
  const updatedHealth = fighter.currentHealth -= damage;
  const healthPercentage = (updatedHealth * 100) / fighter.health;
  
  healthBar.style.width = `${healthPercentage}%`;

  return updatedHealth;
}

function criticalHitTimeOut(fighter){
  fighter.canCriticalHit = false;
  setTimeout(() => {
    fighter.canCriticalHit = true;
  }, 10000);
}