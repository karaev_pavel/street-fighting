import { createElement } from '../helpers/domHelper';

export function createFighterPreview(fighter, position) {
  const positionClassName = position === 'right' ? 'fighter-preview___right' : 'fighter-preview___left';
  const fighterElement = createElement({
    tagName: 'div',
    className: `fighter-preview___root ${positionClassName}`,
  });
  // todo: show fighter info (image, name, health, etc.)
  if(fighter){
    const fighterImage = createFighterImage(fighter);
    const fighterInfo = createFighterDetail(fighter);
    fighterElement.append(fighterImage, fighterInfo);
  }

  return fighterElement;
}

export function createFighterImage(fighter) {
  const { source, name } = fighter;
  const attributes = { 
    src: source, 
    title: name,
    alt: name 
  };
  const imgElement = createElement({
    tagName: 'img',
    className: 'fighter-preview___img',
    attributes,
  });

  return imgElement;
}

function createFighterDetail(fighter){
  const fighterDetail = createElement({
    tagName: 'div',
    className: `fighter-detail`,
  });

  const fighterName = createElement({
    tagName: 'h2',
    className: `fighter-name`,
  });

  fighterName.innerText = fighter.name;

  const fighterHealth = createElement({
    tagName: 'div',
    className: `fighter-health`,
  });

  fighterHealth.innerHTML = `<b>Health:</b> ${fighter.health}`;

  const fighterDefense = createElement({
    tagName: 'div',
    className: `fighter-defense`,
  });

  fighterDefense.innerHTML = `<b>Defense:</b> ${fighter.defense}`;

  const fighterAttack = createElement({
    tagName: 'div',
    className: `fighter-attack`,
  });

  fighterAttack.innerHTML = `<b>Attack:</b> ${fighter.attack}`;

  fighterDetail.append(fighterName, fighterHealth, fighterAttack, fighterDefense);

  return fighterDetail;
}
